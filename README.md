# Ravenpizza

I've written the app in a decoupled frontend/backend style using React (with Redux) and the Django Rest Framework for each part.

## Running the project

If you'd like to run the project, you'll need to install a few requirements beforehand.
In `src/backend/requirements.txt` you'll find the requirements needed by the backend where they can be installed with `pip`.
In `src/frontend/package.json` you'll find the requirements needed by the frontend where they can be installed with `npm install` (or whichever method you prefer).

There's an sqlite db bundled with the project so you won't need to create any data or run any migrations, for the python portion all you need to do is `cd` into `src/backend` and run `./manage.py runserver`. Note that the app assumes the backend is running on port 8000 and you'll encounter problems if it isn't.

The frontend can be started by running `npm start` in the `src/frontend` directory.

### Logging in
You should be able to log in as the user `phil` with the password `password`

## A quick tour of the code

### Backend

Almost all backend logic is contained in the `users` app. I've created a single model, `RPUser`, which extends `AbstractUser` by adding a `likes` field that keeps track of the amount of times a user has liked pizza.

From the frontend what we do for the most is the backend for a list of users and display their attributes in chart form, with the user's name and number of likes as the axes.

Likewise we only really  have one view, `RPUserModelViewSet`, which leverages Django functionality to check for permissions, paginate responses etc.
Aside from this `ObtainAuthToken` was extended to pass down additional user information alongside the token response when authenticating (see note below).

`users.test` contains all of our fixtures and unit / integration tests, which are split down by type (i.e view or model tests).

#### Note on authentication and page refreshes
Given that I've chosen to use token-based authentication for the project, I normally would have stored a refresh token as an http only cookie to persist the session between page refreshes as on the client the access token is stored in memory (as an anti-XSS measure), so would disappear in that case. However because of the need for a certificate to set these cookies cross-domain I forewent this and decided to continue storing the token in memory anyway rather than local storage or methods susceptible to attack.

### Frontend

The frontend's components are organised in the style of having bigger "container" components orchestrate smaller ones inside them.
The container components can be found in `src/components/page/`.

Aside from this we have the following notable directories:
* `action/` contains our actions
* `reducers/` contains our reducers
* `common/` contains type definitions, axios settings, utils etc.
* `css/` contains our stylesheets
* `services/` contains the services used to communicate with the backend
* `root/` contains the root reducer and store used to dispatch actions

I would say the best place to start having a look around would be `components/pages/login.tsx`.

