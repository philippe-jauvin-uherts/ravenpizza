# Python/Django imports
from rest_framework import serializers

# App imports
from .models import RPUser


class RPUserSerializer(serializers.ModelSerializer):
    """
    Serialiser for the RPUser model
    """

    password = serializers.CharField(write_only=True)

    class Meta:
        model = RPUser
        fields = ("id", "username", "likes", "password")
        read_only_fields = ("id", "likes")