from rest_framework import permissions

class RPUserPermission(permissions.BasePermission):
    """
    Permissions class used to determine whether action be performed
    in the RPUser ModelViewSet
    """

    def has_permission(self, request, view):
        """
        Implements check for whether a user can perform an action
        """
        if request.user.is_authenticated:
            return view.action in ('list', 'retrieve', 'partial_update')
        else:
            return view.action == 'create'
        return False


    def has_object_permission(self, request, view, obj):
        """
        Implements check for whether a user can perform an
        action on a specific object
        """
        
        if request.user.is_authenticated:
            if view.action == 'retrieve':
                return True
            elif view.action == 'partial_update':
                return obj == request.user
        return False