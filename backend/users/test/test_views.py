# Python / Django imports
import json
from django.core.exceptions import ValidationError
from django.test import LiveServerTestCase
import requests

# App imports
from users.test.fixtures import RPUserFixture
from users.models import RPUser


class UserViewTests(LiveServerTestCase):
    """
    Test case class for testing User App view functionality
    """

    users_endpoint = "api/user"

    def setUp(self):
        RPUserFixture.generate_test_users()
        self.test_client = requests.session()

        self.first_user_id = RPUser.objects.first().id

        self.first_user_endpoint = "{}/{}".format(self.users_endpoint, self.first_user_id)


    def __common_view_action_restrictions(self, test_client):
        """
        Re-usable test code for checking view permissions
        between unauthenticated and authenticated users
        """

        # Can't delete user
        user_count = RPUser.objects.count()
        response = test_client.delete("{}/{}".format(self.live_server_url, self.first_user_endpoint))
        self.assertTrue(401 <= response.status_code <= 403)
        self.assertEqual(user_count, RPUser.objects.count())

        # Can't create a new user
        response = test_client.post("{}/{}/".format(self.live_server_url, self.users_endpoint),
        data={
            "username":"johnsmith",
            "password":"password123"
        })

        self.assertTrue(401 <= response.status_code <= 403)
        self.assertFalse(RPUser.objects.filter(username="johnsmith").exists())



    def test_unauthenticated_view_actions(self):
        """
        Test to ensure view logic for unautenticated users functions correctly
        """

        # Can't list users
        response = self.test_client.get("{}/{}".format(self.live_server_url, self.users_endpoint))
        self.assertEqual(response.status_code, 401)

        # Can't retrieve user
        response = self.test_client.get("{}/{}".format(self.live_server_url, self.first_user_endpoint))
        self.assertEqual(response.status_code, 401)

        # Can't update user
        response = self.test_client.put("{}/{}".format(self.live_server_url, self.first_user_endpoint),
        data={
            "username":"johnsmith"
        })
        self.assertEqual(response.status_code, 401)
        self.assertFalse(RPUser.objects.filter(username="johnsmith").exists())

        self.__common_view_action_restrictions(self.test_client)        

        # Can log in
        response = self.test_client.post("{}/api/token-auth".format(self.live_server_url),
        data={
            "username":"alex",
            "password":"password123"
        })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get("username"), "alex")
        self.assertEqual(response.json().get("likes"), 4)
        self.assertTrue( "id" in response.json().keys() )
        self.assertTrue( "likes" in response.json().keys() )



    def test_authenticated_view_actions(self):
        """
        Test to ensure view logic for autenticated users functions correctly
        """

        # Log in
        response = self.test_client.post("{}/api/token-auth".format(self.live_server_url),
        data={
            "username":"alex",
            "password":"password123"
        })

        headers = {
            'Authorization': 'Token {}'.format(response.json().get("token")),
            'Content-type': 'application/json'
        }

        self.test_client.headers.update(headers)

        user_id = response.json().get("id")
        
        # Can list users
        response = self.test_client.get("{}/{}".format(self.live_server_url, self.users_endpoint))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get("count"), 7)
        self.assertEqual(len(response.json().get("results")), 5)

        def __check_ordering(response, queryset):

            results = response.json().get("results")

            for index, value in enumerate(results):
                self.assertEqual(queryset[index], value['id'])

        # --- Check id ordering ---
        response = self.test_client.get("{}/{}/?ordering=id".format(self.live_server_url, self.users_endpoint))
        self.assertEqual(response.status_code, 200)

        correct_order = RPUser.objects.order_by('id').values_list('id', flat=True)
        __check_ordering(response, correct_order)

        # --- Check least-to-most likes ordering ---
        response = self.test_client.get("{}/{}/?ordering=likes".format(self.live_server_url, self.users_endpoint))
        self.assertEqual(response.status_code, 200)

        correct_order = RPUser.objects.order_by('likes').values_list('id', flat=True)
        __check_ordering(response, correct_order)

        # --- Check most-to-least likes ordering ---
        response = self.test_client.get("{}/{}/?ordering=-likes".format(self.live_server_url, self.users_endpoint))
        self.assertEqual(response.status_code, 200)

        correct_order = RPUser.objects.order_by('-likes').values_list('id', flat=True)
        __check_ordering(response, correct_order)

        # Can retrieve user
        response = self.test_client.get("{}/{}".format(self.live_server_url, self.first_user_endpoint))
        self.assertEqual(response.status_code, 200)

        put_data = {
            "likes": 999
        }

        # Can their own update user
        response = self.test_client.patch(
            "{}/{}/{}/".format(self.live_server_url, self.users_endpoint, user_id),
            json=put_data
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get("likes"), 999)
        self.assertEqual(response.json().get("username"), "alex")


        # Can't update other users
        response = self.test_client.patch(
            "{}/{}/{}/".format(self.live_server_url, self.users_endpoint, user_id - 1),
            json=put_data
        )

        self.assertEqual(response.status_code, 403)


        # Can get paginated results
        response = self.test_client.get("{}/{}/?page=2".format(self.live_server_url, self.users_endpoint))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json().get("count"), 7)
        self.assertEqual(len(response.json().get("results")), 2)
        

        self.__common_view_action_restrictions(self.test_client)      

