# Python / Django imports
import json
from django.core.exceptions import ValidationError
from django.test import TestCase

# App imports
from users.test.fixtures import RPUserFixture
from users.models import RPUser


class UserModelTests(TestCase):
    """
    Test case class for testing User App model functionality
    """

    def setUp(self):
        RPUserFixture.generate_test_users()


    def test_rpuser_creation(self):
        """
        Test to ensure RPUser records can be created correctly
        """

        try:
            RPUser.objects.create_user(
                username="test",
                password="test",
                likes=5
            )
        except Exception:
            self.fail()


    def test_rpuser_likes_validation(self):
        """
        Test to ensure an RPUser can't have a negative number of likes
        """

        try:
            RPUser.objects.create_user(
                username="test",
                password="test",
                likes=-1
            )
            self.fail()
        except ValidationError:
            pass
        except Exception:
            self.fail()


    def test_rp_user_increment(self):
        """
        Test to ensure that RPUser's likes get incremented correctly
        """

        test_user = RPUser.objects.first()
        initial_likes = test_user.likes

        try:
            test_user.set_likes(-1)
            self.fail()
        except ValueError:
            pass
        except Exception:
            self.fail()

        
        test_user.set_likes(5)
        self.assertEqual(test_user.likes, 5)

        test_user = RPUser.objects.first()
        self.assertEqual(test_user.likes, 5)