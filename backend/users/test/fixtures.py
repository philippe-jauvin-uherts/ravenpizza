# App imports
from users.models import RPUser
from common.constants.testing import DEFAULT_TEST_USER_PASSWORD

class RPUserFixture:
    """
    Utility class for populating our database with RPUser records
    and associated data
    """

    def generate_test_users():
        """
        Generates RPUser records
        """

        RPUser.objects.create_user(
            username="phil",
            password='password',
            likes=1
        )

        RPUser.objects.create_user(
            username="john",
            password=DEFAULT_TEST_USER_PASSWORD,
            likes=2
        )

        RPUser.objects.create_user(
            username="tim",
            password=DEFAULT_TEST_USER_PASSWORD,
            likes=3
        )

        RPUser.objects.create_user(
            username="alex",
            password=DEFAULT_TEST_USER_PASSWORD,
            likes=4
        )

        RPUser.objects.create_superuser(
            username="admin",
            password=DEFAULT_TEST_USER_PASSWORD,
            likes=1000
        )

        RPUser.objects.create_user(
            username="xavier",
            password=DEFAULT_TEST_USER_PASSWORD,
            likes=9
        )

        RPUser.objects.create_user(
            username="zack",
            password=DEFAULT_TEST_USER_PASSWORD,
            likes=9
        )