# Python / Django imports
from rest_framework import viewsets, filters, permissions
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

# App imports
from .models import RPUser
from .permissions import RPUserPermission
from .serializers import RPUserSerializer
from common.constants.voting import GLOBAL_VOTE_INCREMENT


class RPUserModelViewSet(viewsets.ModelViewSet):
    """
    ModelViewSet for RPUser
    """

    lookup_field = "id"
    queryset = RPUser.objects.all()
    serializer_class = RPUserSerializer
    permission_classes = (RPUserPermission,)
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('id', 'likes')
    ordering = ('id',)


@api_view(("POST",))
@permission_classes((permissions.IsAuthenticated,))
def increment_vote(request):
    """
    Simple view that increments the user's votes by some constant
    """
    
    new_likes = request.user.likes + GLOBAL_VOTE_INCREMENT
    request.user.set_likes(new_likes)
    user_data = RPUserSerializer(request.user).data
    return Response(user_data)


class ObtainRPAuthToken(ObtainAuthToken):
    """
    Custom implementation of ObtainAuthToken
    """

    def post(self, request, *args, **kwargs):
        """
        Overridden to send an auth token 
        alongside the newly authenticated user's data
        """

        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)

        user_data = RPUserSerializer(user).data

        user_data.update({
            'token': token.key
        })

        response = Response(user_data)

        return response