# Python / Django imports
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError


class RPUser(AbstractUser):
    """
    AbstractUser implementation that includes the amount
    of times user has liked pizza
    """

    # Amount of times user has liked pizza
    likes = models.IntegerField(default=0)

    def set_likes(self, amount):
        """
        Sets the users likes to the specified amount
        """

        if amount < 0:
            raise ValueError("Likes increment amount must be greater than or equal to 0")

        self.likes = amount
        self.save()

    def clean(self):
        """
        Defines validation rules for the RPUser model
        """

        # Don't allow model to have an amount of likes inferior to 0
        if self.likes < 0:
            raise ValidationError("Likes must be greater than or equal to 0")


    def save(self, *args, **kwargs):
        """
        Defines custom save behaviour for the RPUser model
        """

        # We want validation to run even when saving objects manually
        self.full_clean()
        super(RPUser, self).save(*args, **kwargs)



