# Python/Django imports
from django.urls import path
from rest_framework.routers import DefaultRouter

# App imports
from . import views

router = DefaultRouter()
app_name = "users"

router.register(r"user", views.RPUserModelViewSet, basename="users")

urlpatterns = [
    path('token-auth', views.ObtainRPAuthToken.as_view()),
    path('vote', views.increment_vote)
] + router.urls

