import React from 'react';

import {
  Switch,
  Route,
} from "react-router-dom";

import PrivateRoute from './components/common/private-route';

import HomePage from './components/pages/home';
import LoginPage from './components/pages/login';
import RegisterPage from './components/pages/register';

function App() {
  return (
    <Switch>
      <Route
        path="/login"
        exact
        component={LoginPage}
      />
      <Route
        path="/register"
        exact
        component={RegisterPage}
      />
      <PrivateRoute
        path="/"
        exact
        component={HomePage}
      />
    </Switch>
  );
}

export default App;
