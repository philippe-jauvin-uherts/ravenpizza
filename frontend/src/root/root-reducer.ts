import { combineReducers } from 'redux';

import authReducer from '../reducers/auth-reducer';
import userReducer from '../reducers/user-reducer';

export type CombinedReducerState = {
    auth: any;
}

const rpAppReducer = combineReducers({
    auth: authReducer,
    users: userReducer,
})


export default rpAppReducer;