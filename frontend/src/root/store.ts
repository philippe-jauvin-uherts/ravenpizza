import { createStore } from 'redux';
import rpAppReducer from './root-reducer';

const store = createStore(rpAppReducer);

export default store;