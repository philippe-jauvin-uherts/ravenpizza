// React / TS imports
import axios from 'axios';

// App imports
import AuthService from '../services/auth-service';

// Axios middleware that adds auth token as header to our outgoing requests
axios.interceptors.request.use((config) => {

  // Check if auth token exists in memory and add
  // as header if it does

  const token = AuthService.getStoredAccessToken();

  if(token !== '') {
    config = {
      ...config,
      headers: {
        ...config.headers,
        'Authorization': `Token ${token}`
      }
    }
  }

  return config;

});

// All authenticated requests will be done using exported client
export default axios;