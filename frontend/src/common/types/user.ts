export interface User {
    id: number,
    username: string,
    likes: number
}

export enum UserOrdering {
    MOST_LIKED = '-likes',
    LEAST_LIKED = 'likes',
    ACCOUNT_AGE = 'id'
}