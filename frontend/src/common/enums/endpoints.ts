enum Endpoints {
    USERS = 'api/user/',
    VOTE = 'api/vote',
    LOGIN = 'api/token-auth',
}

export default Endpoints;