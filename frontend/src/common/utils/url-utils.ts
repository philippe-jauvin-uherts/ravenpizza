// App imports
import apiUrl from '../constants/api';
import Endpoints from '../enums/endpoints';

// Utility class for making URL formatting easier
export default class UrlUtils {

    // Returns a nicely formatted URL including the provided endpoint
    static getUrl(endpoint: Endpoints, resourceId: number = null): string {

        const url = `${apiUrl}${endpoint}`;

        if(resourceId) {
            return `${url}${resourceId}/`;
        }

        return url;

    }

}