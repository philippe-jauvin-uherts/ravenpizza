import UserActions from '../actions/user-actions';
import UserService from '../services/user-service';
import { User } from '../common/types/user';

interface UserState {
    users: Array<User>,
    hasNext: boolean,
    currentUser?: User,
    userPage: number;
}

const defaultState: UserState = {
    users: [],
    hasNext: false,
    currentUser: null,
    userPage: 1,
}

export default function userReducer(state = defaultState, action: any) {
    switch (action.type) {

        case UserActions.FETCH_USERS: {
            UserService.fetchUsers(action.payload.page, action.payload.ordering);
            return state;
        }

        case UserActions.FETCH_USERS_SUCCESS: {

            const users = action.payload.users;
            const hasNext = action.payload.hasNext;
            const userPage = action.payload.newPage;

            return {
                ...state,
                users,
                hasNext,
                userPage
            }
        }

        case UserActions.FETCH_USERS_FAILURE: {
            alert("Error fetching users :(");
            return state;
        }

        case UserActions.VOTE: {
            UserService.vote();
            return state;
        }

        case UserActions.VOTE_FAILURE: {
            alert("Error voting :(");
            return state;
        }

        case UserActions.SET_USER: {

            const currentUser = action.payload.user;

            if(state.users) {
                for( let i=0; i < state.users.length; i++ ){
                    if( state.users[i].id === currentUser.id){
                        state.users[i] = {...currentUser};
                    }
                }
            }

            return  {
                ...state,
                currentUser
            }
        }

        default:
            return state

    }
}