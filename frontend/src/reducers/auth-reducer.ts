import AuthActions from '../actions/auth-actions';
import AuthService from '../services/auth-service';

const defaultState = {
    success: false,
}

export default function authReducer(state = defaultState, action: any) {
    switch (action.type) {

        case AuthActions.LOGIN: {

            AuthService.login(
                action.payload.username,
                action.payload.password
            );

            return state;
        }

        case AuthActions.LOGIN_SUCCESS: {
            return {
                ...state,
                success: true,
            }
        }

        case AuthActions.LOGIN_FAILURE: {
            alert("Error logging in :(");
            return state;
        }

        case AuthActions.REGISTER: {

            AuthService.register(
                action.payload.username,
                action.payload.password
            );

            return state;

        }

        case AuthActions.REGISTER_FAILURE: {
            alert("Error registering :(");
            return state;
        }

        case AuthActions.LOGOUT: {
            AuthService.logout();
            return state;
        }

        default:
            return state;
    }
}