// App imports
import { User, UserOrdering } from '../common/types/user';

enum UserActions {
    FETCH_USERS = 'FETCH_USERS',
    FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS',
    FETCH_USERS_FAILURE = 'FETCH_USERS_FAILURE',
    VOTE = 'VOTE',
    VOTE_SUCCESS = 'VOTE_SUCCESS',
    VOTE_FAILURE = 'VOTE_FAILURE',
    SET_USER = 'SET_USER'
}

export function fetchUsers(page = 1, ordering: UserOrdering): any {
    return {
        type: UserActions.FETCH_USERS,
        payload: { page, ordering }
    }
}

export function fetchUsersSuccess(users: Array<User>, hasNext: boolean, newPage: number): any {
    return {
        type: UserActions.FETCH_USERS_SUCCESS,
        payload: { users, hasNext, newPage }
    }
}

export function fetchUsersFailure(): any {
    return {
        type: UserActions.FETCH_USERS_FAILURE,
    }
}

export function vote(): any {
    return {
        type: UserActions.VOTE
    }
}

export function voteSuccess(): any {
    return {
        type: UserActions.VOTE_SUCCESS
    }
}

export function voteFailure(): any {
    return {
        type: UserActions.VOTE_FAILURE,
    }
}

export function setUser(user: User): any {
    return {
        type: UserActions.SET_USER,
        payload: { user }
    }
}

export default UserActions;
