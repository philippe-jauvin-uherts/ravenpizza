enum AuthActions {
    LOGIN = 'LOGIN',
    LOGIN_SUCCESS = 'LOGIN_SUCCESS',
    LOGIN_FAILURE = 'LOGIN_FAILURE',
    REGISTER = 'REGISTER',
    REGISTER_SUCCESS = 'REGISTER_SUCCESS',
    REGISTER_FAILURE = 'REGISTER_FAILURE',
    LOGOUT = 'LOGOUT',
}


export function login(username: string, password: string): any {
    return {
        type: AuthActions.LOGIN,
        payload: {
            username,
            password
        }
    }
}

export function loginSuccess(): any {
    return {
        type: AuthActions.LOGIN_SUCCESS,
    }
}


export function loginFailure(): any {
    return {
        type: AuthActions.LOGIN_FAILURE,
    }
}


export function register(username: string, password: string): any {
    return {
        type: AuthActions.REGISTER,
        payload: {
            username,
            password
        }
    }
}


export function registerSuccess(): any {
    return {
        type: AuthActions.REGISTER_SUCCESS,
    }
}


export function registerFailure(): any {
    return {
        type: AuthActions.REGISTER_FAILURE,
    }
}


export function logout(): any {
    return {
        type: AuthActions.LOGOUT,
    }
}

export default AuthActions;
