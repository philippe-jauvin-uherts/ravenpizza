// React / TS improts
import React from 'react';
import { connect } from "react-redux";

// App imports
import BarChart from '../common/barchart';
import { fetchUsers, vote } from '../../actions/user-actions';
import { logout } from '../../actions/auth-actions';
import { User, UserOrdering } from '../../common/types/user';

// CSS imports
import '../../css/home.css';


// Define types for our props
interface HomeProps {
    users: Array<User>;
    hasNext: boolean;
    currentUser: User;
    userPage: number;
    fetchUsers: Function;
    vote: Function;
    logout: Function;
}


// Define types for our state
interface HomeState {
    ordering: UserOrdering;
    likeMessage: string;
    voteMessage: string;
}


const likeMessage = "You've liked pizza :)";
const voteMessage = "Do it again !";

class HomePage extends React.Component<HomeProps, HomeState> {

    constructor(props: any) {

        super(props);

        const likes = this.props.currentUser.likes;

        this.state = {
            ordering: UserOrdering.MOST_LIKED,
            likeMessage: likes ? likeMessage : "You haven't liked pizza yet :(",
            voteMessage: likes ? voteMessage : "I like it !"
        }

        this._previousPage = this._previousPage.bind(this);
        this._nextPage = this._nextPage.bind(this);
        this._vote = this._vote.bind(this);
        this._reOrder = this._reOrder.bind(this);
        this._logout = this._logout.bind(this);

    }

    componentDidMount(): void {
        this.props.fetchUsers(this.props.userPage, this.state.ordering);
    }

    componentDidUpdate(prevProps, prevState, snapshot){

        const previousLikes = prevProps.currentUser.likes;
        const currentLikes =  this.props.currentUser.likes;

        if(previousLikes === 0 && currentLikes === 1){
            this.setState({
                likeMessage: likeMessage,
                voteMessage: voteMessage
            })
        }
        
    }

    render(): JSX.Element {

        return (
            <div>
                <div className="home-container">
                    <BarChart 
                    labels={this.props.users.map(user => user.username)}
                    dataPoints={this.props.users.map(user => user.likes)} />
                    <div className="nav-buttons">
                        <span className={this.props.userPage === 1 ? 'hide-button' : ''} onClick={this._previousPage}>Previous</span>
                        <select onChange={this._reOrder} value={this.state.ordering}>
                            <option value={UserOrdering.MOST_LIKED}>Most liked</option>
                            <option value={UserOrdering.LEAST_LIKED}>Least liked</option>
                            <option value={UserOrdering.ACCOUNT_AGE}>Account age</option>
                        </select>
                        <span className={this.props.hasNext ? '': 'hide-button'} onClick={this._nextPage}>Next</span>
                    </div>
                </div>
                <div>
                    <div className="vote-box">
                        <div>
                            <span>{this.state.likeMessage} ({this.props.currentUser.likes})</span>
                        </div>
                        <div>
                            <span className="clickable" onClick={this._vote}>{this.state.voteMessage}</span>
                        </div>
                    </div>
                    <div className="clickable">
                        <span onClick={this._logout}>I'd like to logout, actually</span>
                    </div>
                </div>
            </div>
        );

    }


    // Navigates to the previous page of users
    private _previousPage(): void {
        const previousPage = this.props.userPage - 1;
        this.props.fetchUsers(previousPage, this.state.ordering);
    }


    // Navigates to the next page of users
    private _nextPage(): void {
        const nextPage = this.props.userPage + 1;
        this.props.fetchUsers(nextPage, this.state.ordering);
    }


    // Increments the user's vote count
    private _vote(): void {
        this.props.vote();
    }


    // Logs a user out of their session
    private _logout(): void {
        this.props.logout();
    }


    // Re-initialises the user listing with a new order
    private _reOrder(event): void {

        const newOrder = event.target.value;

        this.props.fetchUsers(1, newOrder);

        this.setState({
            ordering: newOrder
        })

    }


}


const mapDispatchToProps = (dispatch: Function) => {
    return {
      fetchUsers: (page: number, ordering: UserOrdering) => dispatch(fetchUsers(page, ordering)),
      vote: () => dispatch(vote()),
      logout: () => dispatch(logout()),
    }
}

const mapStateToProps = (state: any) => {
    const { users, hasNext, currentUser, userPage } = state.users;
    return {
        users,
        hasNext,
        currentUser,
        userPage
    }
}

export default connect(
    mapStateToProps, mapDispatchToProps)(
    HomePage
)