// React / TS imports
import React from 'react';
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

// App imports
import { login } from '../../actions/auth-actions';
import Credentials from '../common/credentials';

// CSS imports
import '../../css/login.css';

// Define types for our props
interface LoginProps {
    success: boolean;
    login: Function;
}

// Component displaying a login form
class LoginPage extends React.Component<LoginProps> {

    render(): JSX.Element {
        // If login was a success, redirect the user
        // Otherwise continue to show the Credentials component
        return (
            <div className="login-container">
                <span className="login-icon"></span>
                <span>Ravenpizza login</span>
                { this.props.success && this._redirectOnSuccess() }
                { !this.props.success && <Credentials onSubmit={this.props.login} />}
                <a href="/register" className="clickable">Sign up for Ravenpizza</a>
            </div>
        );
    }

    private _redirectOnSuccess(): JSX.Element {
        return (<Redirect to='/' />);
    }

}

const mapDispatchToProps = (dispatch: Function) => {
    return {
      login: (username: string, password: string) => dispatch(login(username, password)),
    }
}

const mapStateToProps = (state: any) => {
    const { success } = state.auth;
    return {
        success
    }
}

export default connect(
    mapStateToProps, mapDispatchToProps)(
    LoginPage
)