// React / TS imports
import React from 'react';
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

// App imports
import { register } from '../../actions/auth-actions';
import Credentials from '../common/credentials';

// CSS imports
import '../../css/login.css';

// Define types for our props
interface RegisterProps {
    success: boolean;
    register: Function;
}

// Component displaying a login form
class RegisterPage extends React.Component<RegisterProps> {

    render(): JSX.Element {
        // If registration was a success, redirect the user
        // Otherwise continue to show the Credentials component
        return (
            <div className="login-container">
                <span className="login-icon"></span>
                <span>Ravenpizza signup</span>
                { this.props.success && this._redirectOnSuccess() }
                { !this.props.success && <Credentials shouldConfirmPassword={true} onSubmit={this.props.register} />}
                <a href="/login" className="clickable">Go to sign in</a>
            </div>
        );
    }

    private _redirectOnSuccess(): JSX.Element {
        return (<Redirect to='/' />);
    }

}

const mapDispatchToProps = (dispatch: Function) => {
    return {
      register: (username: string, password: string) => dispatch(register(username, password)),
    }
}

const mapStateToProps = (state: any) => {
    const { success } = state.auth;
    return {
        success
    }
}

export default connect(
    mapStateToProps, mapDispatchToProps)(
    RegisterPage
)