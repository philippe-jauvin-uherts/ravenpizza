import React, { SyntheticEvent } from 'react';

// Define types for our state properties
interface CredentialsState {
    username: string;
    password: string;
    confirmPassword: string;
}

// Define types for our props
interface CredentialsProps {
    shouldConfirmPassword?: boolean;
    onSubmit: Function;
}

// Define fields that we can set the value for
enum CredentialsFields {
    USERNAME = 'username',
    PASSWORD = 'password',
    PASSWORD_CONFIRM = 'confirm_password'
}

class Credentials extends React.Component<CredentialsProps, CredentialsState> {

    constructor(props: any) {

        super(props);

        this.state = {
            username: '',
            password: '',
            confirmPassword: '',
        }

        this._setFieldValue = this._setFieldValue.bind(this);
        this._submit = this._submit.bind(this);

    }

    render(): JSX.Element {

        return (
            <div>
                <input placeholder="username" type="text" name="username" value={this.state.username} onChange={this._setFieldValue} />
                <input placeholder="password" type="password" name="password" value={this.state.password} onChange={this._setFieldValue} />
                { this._renderPasswordConfirmation() }      
                <button onClick={this._submit}>Submit</button>
            </div>
        );

    }

    // Dynamically set value of form fields
    private _setFieldValue(event: SyntheticEvent): void {

        const target = event.target as HTMLButtonElement;

        switch(target.name){
            case CredentialsFields.USERNAME:
                this.setState({username: target.value});
                break;
            case CredentialsFields.PASSWORD:
                this.setState({password: target.value});
                break;
            case CredentialsFields.PASSWORD_CONFIRM:
                this.setState({confirmPassword: target.value});
                break;
        }
        
    }


    private _renderPasswordConfirmation(): JSX.Element {
        
        if(!this.props.shouldConfirmPassword) return;

        return (
            <input placeholder="confirm password" type="password" name="confirm_password" value={this.state.confirmPassword} onChange={this._setFieldValue} />
        );

    }


    private _submit(): void {

        if(this.props.shouldConfirmPassword) {
            if(this.state.password !== this.state.confirmPassword) {
                alert('Passwords must match !');
                return;
            }
        }

        this.props.onSubmit(this.state.username, this.state.password);
    }

}

export default Credentials;