// React / TS imports
import React from 'react'
import { Bar } from 'react-chartjs-2';

// Define types for our props
interface BarChartProps {
    labels: Array<string>;
    dataPoints: Array<number>;
}


// Wrapper for the BarChart component
export default class BarChart extends React.Component<BarChartProps> {

    render(): JSX.Element {

        const chartData = {
            labels: this.props.labels,
            datasets: [
              {
                label: 'User likes',
                data: this.props.dataPoints,
                backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                ],
                borderColor: [
                  'rgba(255, 99, 132, 1)',
                ],
                borderWidth: 1,
              },
            ],
        }

        return (
            <div>
                <Bar type={'bar'} data={chartData}/>
            </div>
        );
    }

}