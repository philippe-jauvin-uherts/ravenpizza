// React / TS imports
import React from 'react';
import { Redirect, Route } from 'react-router-dom';

// App imports
import AuthService from '../../services/auth-service';


// Custom component for redirecting an unauthenticated user from this route to the login page
const PrivateRoute = ({ component: Component, ...rest }) => {

  const isLoggedIn = AuthService.isLoggedIn();

  return (

    <Route
      {...rest}
      render={props =>
        isLoggedIn ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        )
      }
    />
  )
}

export default PrivateRoute