// App imports
import Endpoints from '../common/enums/endpoints';
import UrlUtils from '../common/utils/url-utils';
import { login, loginSuccess, loginFailure, registerFailure } from '../actions/auth-actions';
import { setUser } from '../actions/user-actions';
import axios from '../common/api-client';
import { User } from '../common/types/user';
import store from '../root/store';


// Closure used for storing authentication tokens
// in memory - we don't want to be using localStorage or JS accessible cookies
export const JWTClosure = (() => {

    let storedToken = '';
 
    const getToken = () => {
        return storedToken;
    };
 
    const setToken = (token: string) => {
        storedToken = token;
    };
 
    const deleteToken = () => {
        storedToken = '';
    };
 
    return {
        getToken,
        setToken,
        deleteToken,
    };

})();
 

// Service providing Authentication related functionality
export default class AuthService {

    // Logs a user into the system
    public static login(username: string, password: string): void {

        const params = {
            username,
            password
        }

        const url = UrlUtils.getUrl(Endpoints.LOGIN);

        axios.post(url, params).then( (response) => {
            if(response.status === 200) {
                JWTClosure.setToken(response.data.token);

                const currentUser: User = {
                    username: response.data.username,
                    likes: response.data.likes,
                    id: response.data.id
                }

                store.dispatch(setUser(currentUser));
                store.dispatch(loginSuccess());
            }
        } ).catch( (error) => {
            store.dispatch(loginFailure());
        } );

    }


    // Registers a new user
    public static register(username: string, password: string): void {

        const params = {
            username,
            password
        }

        const url = UrlUtils.getUrl(Endpoints.USERS);

        axios.post(url, params).then( (response) => {
            if(response.status === 201) {
                store.dispatch(login(username, password));
            }
        } ).catch( (error) => {
            store.dispatch(registerFailure());
        } );


    }


    // Logs a user out of the system
    public static logout(): void {
        JWTClosure.deleteToken();
        window.location.reload();
    }


    // Returns a boolean indicating whether the user is logged in
    public static isLoggedIn(): boolean {
        return JWTClosure.getToken() !== '';
    }


    // Returns the access token used for authentication
    public static getStoredAccessToken(): string {
        return JWTClosure.getToken();
    }


}