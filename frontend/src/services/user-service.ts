// TS / React imports
import axios from '../common/api-client';

// App imports
import Endpoints from '../common/enums/endpoints';
import UrlUtils from '../common/utils/url-utils';
import { fetchUsersSuccess, fetchUsersFailure, setUser, voteFailure } from '../actions/user-actions';
import { User, UserOrdering } from '../common/types/user';

import store from '../root/store';
 

// Service providing User related functionality
export default class UserService {

    // Logs a user into the system
    public static fetchUsers(page = 1, ordering: UserOrdering): void {

        const params = { page, ordering }

        const url = UrlUtils.getUrl(Endpoints.USERS);

        axios.get(url, {params}).then( (response) => {
            if(response.status === 200) {

                const users = response.data.results as Array<User>;
                const hasNext = response.data.next !== null;

                store.dispatch(fetchUsersSuccess(users, hasNext, page));
            }
        } ).catch( (error) => {
            store.dispatch(fetchUsersFailure());
        } );

    }

    
    // Updates vote count
    public static vote(): void {

        const url = UrlUtils.getUrl(Endpoints.VOTE);

        axios.post(url).then( (response) => {
            if(response.status === 200) {
                store.dispatch(setUser(response.data as User));
            }
        } ).catch( (error) => {
            store.dispatch(voteFailure());
        } );

    }


}